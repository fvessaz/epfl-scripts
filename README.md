# EPFL Scripts

Scripts that are useful when you spend time at EPFL.


## Installation

After installing the dependencies (see below), the various script can either
be installed somewhere on your system or run directly from the source tree.

If you want to install them (by copying them or creating symlinks), you can
use the `install.sh` script.

For Arch Linux, we also provide a PKGBUILD in the [`archlinux`](/archlinux)
directory, which installs both the scripts and the required dependencies.


## Dependencies

For the shell scripts, the following software must be installed on your system:

  * coreutils
  * curl
  * file
  * openconnect
  * vpnc/vpnc-script
  * POSIX-compatible shell (dash/ash, zsh, bash, ...)

For the Perl scripts, the following modules must be installed on your system:

  * File::MimeInfo::Magic
  * HTML::TreeBuilder::XPath
  * HTML::TreeBuilder
  * IO::Scalar
  * LWP::UserAgent
  * Term::ANSIColor
  * WWW::Mechanize

For Debian (or Debian-based distributions like Ubuntu), the Perl modules are
provided by the following packages:

  * libfile-mimeinfo-perl
  * libhtml-treebuilder-xpath-perl
  * libhtml-tree-perl
  * libio-stringy-perl
  * libwww-perl
  * perl-modules
  * libwww-mechanize-perl

For Arch Linux, the Perl modules are provided by the following packages:

  * perl-www-mechanize
  * perl-html-tree
  * perl-html-treebuilder-xpath (AUR)
  * perl-xml-xpathengine (AUR)
  * perl-lwp-protocol-https
  * perl-io-stringy


## Contributing

To ensure robustness, pushing directly to the `epfl-scripts` repository is not
permitted. When contributing, please fork `epfl-scripts` and make your changes
in your forked repository, then open a merge request, so that your changes can
undergo a brief review and feedback cycle before being applied.

Please note the following when opening a merge request:

* For all changes in the source code, the version information in the
  distribution-specific packaging files (see below) must be updated. The version
  value is the current date in the `%Y.%m.%d` format. If the changes only affect
  the packaging files themselves (or if there are multiple code changes in one
  day), the package-*release* should be incremented instead (default: 1).

* If your changes introduce new dependencies or remove some, the [dependency
  list](#dependencies) in the Readme and the distribution-specific packaging
  files must be updated.

Currently, we provide packaging files for Arch Linux (in
[`archlinux`](/archlinux)). If you do not know or use that distribution, feel
free to simply note that in the merge request; somebody else can then make the
necessary changes.

For Arch Linux, please note the following:

* `makepkg` currently does not provide any way to build native packages like
  some other packaging tools, and files other than the ones in the same
  directory as the PKGBUILD cannot be referenced. We circumvent this limitation
  by adding symbolic links to the script files in `bin` to the `archlinux`
  directory. If any scripts are added or removed, the symbolic links (and the
  `source` array in the PKGBUILD) must be adapted accordingly.

* If package dependencies are added or removed, the `depends` array in the
  PKGBUILD must be adapted accordingly.
