#!/usr/bin/env perl
# Copyright 2014 Florian Vessaz
# Copyright 2015 Tinu Weber
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
use strict;
use warnings;
use 5.10.0;
use WWW::Mechanize;
use HTML::TreeBuilder;
use HTML::TreeBuilder::XPath;
use Term::ANSIColor;

my $url = "https://mycamipro.epfl.ch/client/mobile/?p=velopass";
my $highlight = 'Piccard|Turing|Rivier|nord';
my $color_available = 'bold';

my $mech = WWW::Mechanize->new(autocheck => 1);
$mech->get($url);

my ($VELOS, $BORNES, $STATION) = (1, 2, 0);

my $tree = HTML::TreeBuilder->new;
$tree->parse($mech->content(decoded_by_headers => 1));
$tree->eof;

my @res = $tree->findnodes('/html/body//tr');
my $velos_total = 0;
foreach my $row (@res) {
	my @infos = ();
	foreach my $e ($row->content_list) {
		push @infos, $e->as_text;
	}
	{
		no warnings 'numeric';
		if ((int $infos[$VELOS] // 0) != 0) {
			print color($color_available);
		}
	}
	printf '%4$1s%2$5s %3$6s %1$s' . color('reset') . "\n",
			@infos, $infos[$STATION] =~ m/$highlight/ ? '>' : ' ';
	$velos_total += int $infos[$VELOS];
}
printf "\n" . colored(['bold'], ' %1$5s %2$6s Total' . "\n"), ($velos_total, '')
