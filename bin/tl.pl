#!/usr/bin/env perl
# Copyright 2014 Florian Vessaz
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
use strict;
use warnings;
use WWW::Mechanize;
use HTML::TreeBuilder;
use HTML::TreeBuilder::XPath;
use Term::ANSIColor;

if (defined $ARGV[0] and $ARGV[0] =~ /renens/i) {
	dump_times("http://www.t-l.ch/tl-live-mobile/line_detail.php?from=horaire&id=3377704015495891&line=11821953316814882&id_stop=2533279085394610&id_direction=11821953316814882&lineName=m1");
} else {
	dump_times("http://www.t-l.ch/tl-live-mobile/line_detail.php?from=horaire&id=3377704015495520&line=11821953316814882&id_stop=2533279085527824&id_direction=11821953316814882&lineName=m1");
	dump_times("http://www.t-l.ch/tl-live-mobile/line_detail.php?from=horaire&id=3377704015495518&line=11821953316814882&id_stop=2533279085529066&id_direction=11821953316814882&lineName=m1");
}

sub dump_times {
	my $url = shift;
	my $mech = WWW::Mechanize->new(autocheck => 1);
	$mech->get($url);

	my $tree = HTML::TreeBuilder->new;
	$tree->parse($mech->content(decoded_by_headers => 1));
	$tree->eof;

	my $line = $tree->findnodes('/html/body//div[@id="ListHeaderShortName"]');
	$line = $line->string_value;
	$line =~ s/^\s*(.*?)\s*$/$1/;
	print color('bold') . $line . color('reset') . " ";

	my $stops = $tree->findnodes(
		'/html/body//div[@id="ListHeaderLongName"]//text()');
	my $stop = $stops->shift->string_value;
	my $destination = $stops->shift->string_value;
	print color('blue') . "$stop -> $destination" . color('reset') . "\n";

	my $alerts = $tree->findnodes('/html/body//div[@id="alert"]//text()');
	if ($alerts->size > 0) {
		print color 'red';
		print " !! $_\n" foreach $alerts->string_values;
		print color 'reset';
	}

	my $times = $tree->findnodes('/html/body//div[@class="time"]//text()');
	print " ";
	print "$_  " foreach $times->string_values;
	print "\n\n";
}
